for (i = 1; i <= 20; i++) {
    var tCount = document.createElement("div");
    var count = document.createTextNode(i);
    tCount.appendChild(count);
    var tSend = document.getElementById("one_twenty");
    tSend.appendChild(tCount);

}

for (i = 1; i <= 20; i++) {
    if (i % 2 != 0) { continue;}
    var eCount = document.createElement("div");
    var c = document.createTextNode(i);
    eCount.appendChild(c);
    var eSen = document.getElementById("even_twenty");
    eSen.appendChild(eCount);
}

for (i = 1; i <= 20; i++) {
    if (i % 2 === 0) { continue;}
    var oCount = document.createElement("div");
    var o = document.createTextNode(i);
    oCount.appendChild(o);
    var oSend = document.getElementById("odd_twenty");
    oSend.appendChild(oCount);
}

var fives = ""
for (i = 5; i <= 100; i = i + 5) {
    fives = fives + i + ", ";

}
var f = document.createElement("div");
var fText = document.createTextNode(fives);
f.appendChild(fText);
var fiveHome = document.getElementById("five_mult");
fiveHome.appendChild(f);

var squares = "";
for (i = 1; i <= 10; i++) {
    squares = squares + (i*i) + ", ";
}
var sqMake = document.createElement("div");
var sqText = document.createTextNode(squares);
sqMake.appendChild(sqText);
var sqHome = document.getElementById("squares");
sqHome.appendChild(sqMake);

for (i = 20; i > 0; i--) {
    var bc = document.createElement("div");
    var bcount = document.createTextNode(i);
    bc.appendChild(bcount);
    var bcHome = document.getElementById("backwards");
    bcHome.appendChild(bc);
}

for (i = 20; i > 0; i--) {
    if (i % 2 != 0) { continue;}
    var ebc = document.createElement("div");
    var ebcount = document.createTextNode(i);
    ebc.appendChild(ebcount);
    var ebcHome = document.getElementById("even_back");
    ebcHome.appendChild(ebc);
}

for (i = 20; i >= 0; i--) {
    if (i % 2 === 0) { continue;}
    var obc = document.createElement("div");
    var obcount = document.createTextNode(i);
    obc.appendChild(obcount);
    var obcHome = document.getElementById("odd_back");
    obcHome.appendChild(obc);
}

for (i = 100; i > 0; i = i - 5) {
    var bfc = document.createElement("div");
    var bfcount = document.createTextNode(i);
    bfc.appendChild(bfcount);
    var bfcHome = document.getElementById("five_back");
    bfcHome.appendChild(bfc);
}

for (i = 10; i > 0; i--) {
    var bsq = document.createElement("div");
    var bsqcount = document.createTextNode(i * i);
    bsq.appendChild(bsqcount);
    var bsqHome = document.getElementById("square_back");
    bsqHome.appendChild(bsq);
}

const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

for (i = 0; i < 20; i++) {
    var x = sampleArray[i];
    var sam = document.createElement("div");
    var sami = document.createTextNode(x);
    sam.appendChild(sami);
    var samHome = document.getElementById("sample1");
    samHome.appendChild(sam);
}

for (i = 0; i < sampleArray.length; i++) {
    var v = sampleArray[i];
    if (v % 2 != 0) { continue;}
    var esam = document.createElement("div");
    var esamNum = document.createTextNode(v);
    esam.appendChild(esamNum);
    var esamHome = document.getElementById("sample_even");
    esamHome.appendChild(esam);
}

for (i = 0; i < sampleArray.length; i++) {
    var v = sampleArray[i];
    if (v % 2 === 0) { continue;}
    var osam = document.createElement("div");
    var osamNum = document.createTextNode(v);
    osam.appendChild(osamNum);
    var osamHome = document.getElementById("sample_odd");
    osamHome.appendChild(osam);
}

for (i = 0; i < sampleArray.length; i++) {
    var s = sampleArray[i] * sampleArray[i];
    var sqar = document.createElement("div");
    var sqarNum = document.createTextNode(s);
    sqar.appendChild(sqarNum);
    var sqarHome = document.getElementById("sq_sample");
    sqarHome.appendChild(sqar);
}
var m = 0;
for (i = 0; i <= 20; i++) {
    m = i + m;
}
var sm = document.createElement("div");
var smn = document.createTextNode(m);
sm.appendChild(smn);
var smHome = document.getElementById("sum20");
smHome.appendChild(sm);

var z = 0;
for (i = 0; i < sampleArray.length; i++) {
    z = z + sampleArray[i];
}
var sumSam = document.createElement("div");
var sumSamNum = document.createTextNode(z);
sumSam.appendChild(sumSamNum);
var sumSamHome = document.getElementById("sumSample");
sumSamHome.appendChild(sumSam);

var minx = Math.min(...sampleArray);
var minE = document.createElement("div");
var minNum = document.createTextNode(minx);
minE.appendChild(minNum);
var minHome = document.getElementById("mini");
minHome.appendChild(minE);

var maxx = Math.max(...sampleArray);
var maxE = document.createElement("div");
var maxNum = document.createTextNode(maxx);
maxE.appendChild(maxNum);
var maxHome = document.getElementById("maxi");
maxHome.appendChild(maxE);